import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class WriteObject {
	public static void main(String[] args) {

		Map<Integer, Object> map = new HashMap<Integer, Object>();

		Person person1 = new Person("God", "Vlad");
		Person person2 = new Person("Sensei", "Radu");
		Person person3 = new Person("Guru", "Andrei");
		Person person4 = new Person("Magistru", "Alex");
		Person person5 = new Person("Alpha", "Eugen");

		map.put(1, person1);
		map.put(2, person2);
		map.put(3, person3);
		map.put(4, person4);
		map.put(5, person5);

		
		try {
			FileOutputStream fos = new FileOutputStream("people.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(map);
			// oos.writeObject(map.size());
			//
			// for (Entry<Integer, Person> entry : map.entrySet()) {
			// oos.writeObject(map);
			// }
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}