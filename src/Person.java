import java.io.Serializable;

public class Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7235661064763529383L;
	private String lvl;
	private String name;

	public Person(String lvl, String name) {
		this.lvl = lvl;
		this.name = name;
	}

	public String getId() {
		return lvl;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return "Lvl -> " + lvl + " - " + "Name : " + name + ",\n";
	}
}