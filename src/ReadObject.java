import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class ReadObject {
	public static void main(String[] args) {

		Map<Integer, Object> map = new HashMap<Integer, Object>();

		try {
			FileInputStream fis = new FileInputStream("people.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);

			map = (Map<Integer, Object>) ois.readObject();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Scanner sc = new Scanner(System.in);
		Set<Integer> keySet = map.keySet();
		System.out.println("Please select a nr. from this range, " + keySet.toString());
		int userInput = sc.nextInt();

		System.out.println(map.get(userInput));
	}
}
